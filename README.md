# Maze Machine

## What it is
This is the code for a uni assignment, where an Arduino (or, in this case, two)
needs to interface with someone else's project to control a marble maze from
controllers connected to both machines.

## Running it
Complicated. This is more of a "for reference" than a "try it yourself" kind
of project, but if you feel like getting this going yourself, you will need:
 - An Arduino Mega 2560 (or any ATmega2560 development board)
 - An Arduino Micro (or any ATmega32U4 Development board)
 - 2 Servo motors
 - A 9-DOF IMU
 - An I<sup>2</sup>C controlled LCD Display (20x4)
 - A bunch of buttons
 - A custom PCB (unless you're rad with a bradboard)
 - A LDR
 - Power supply for all the components
 - A whole bunch of other things that I haven't listed here

## CAD Files
(Coming soon. Once I work out how "CAD" works with "GIT".) All the cad files have
been created using Autodesk Inventor, but there are also a bunch of AutoCAD
.dwg files for the laser cutting, some .stl files for the 3D printed parts, and
the PCB was designed in Autodesk Eagle, with some output gerber files for giving
to anything that can mill PCBs.

## License
This is one of those "I can't be bothered choosing a license" kind of licenses.
Do what you want as long as it doesn't come back to me. I don't want to deal
with warranties, complaints, recommendations, patents, or any other BS. You
can do whatever you see fit with this code as long as none of it is traceable
back to me. (Unless I get around to uploading a LICENSE.md, in which case you
should probably obey that....)
