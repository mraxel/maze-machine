﻿/*
 Name:				Maze Machine.ino
 Created:			2018-08-25 14:56:32
 Author:			Alexander Gregory
 Student ID:		217270644
 Unit Code:			SER201
 Unit Name:			Embedded System Design
 Unit Chair:		Ben Champion
 Comments:			Definitions are in ALL CAPS, variables are in camelCaseWithLeadingLowercase, functions are in lowercase_with_underscores(if_applicible).
*/

// Include required libraries:
#include <Wire.h>						/// For communication over I2C
#include <LiquidCrystal_I2C.h>			/// For I2C control of LCD display
#include <MPU9250_asukiaaa.h>			/// For I2C reading of the IMU unit
#include <Servo.h>						/// For controlling the servos

// Define I2C Addresses
#define I2C_SLAVE 0x09
#define I2C_IMU 0x00
#define I2C_LCD 0x27

// Define Serial data
#define END_FRAME 0x80

// Define Debug Information
#define DEBUG_SERIAL true
#define DEBUG_INTERVAL 1000

// Define pins
#define LDR 15							/// This is an analogue pin
#define LASER 8
#define SERVO_PITCH 7
#define SERVO_ROLL 6

// Define conversaion scales for servo output
//#define SCALE_ROLL 90/64
//#define SCALE_PITCH 30/64
#define SCALE_ROLL 1.40625
#define SCALE_PITCH 0.5

// Define game information
#define TIME_NORMAL 10000
#define TIME_CHALLENGE 5000

// Setup the LCD Display
LiquidCrystal_I2C lcd(I2C_LCD, 20, 4);	/// I2C address, cols, rows
uint8_t oldScreen = 1;					/// Used to know what the screen was displaying, so we aren't continually writing to it.

// Setup the IMU
MPU9250 imu;
uint8_t SensorID;
float accelX, accelY;					/// Variables for storing values from the IMU
char stringAccelX[5], stringAccelY[5];	/// Variables for printing values from the IMU

// Setup the two servos
Servo servoPitch;
Servo servoRoll;

// Setup the servo definitions
const uint8_t rangeRoll = 180;
const uint8_t rangePitch = 60;
const uint8_t limitLowerPitch = 90 - (rangePitch / 2);
const uint8_t limitUpperPitch = 90 + (rangePitch / 2);
const uint8_t limitLowerRoll = 90 - (rangeRoll / 2);
const uint8_t limitUpperRoll = 90 + (rangeRoll / 2);
float sensitivity = 0.75;

// Variables for Serial RX & TX
uint8_t SerialTX_control = 0;	/// Variable for organising control packet before transmission
uint8_t SerialTX_roll = 0;		/// Variable for organising roll packet before transmission (or score part 1)
uint8_t SerialTX_pitch = 0;		/// Variable for organising pitch packet before transmission (or score part 2)
uint8_t SerialTX_score_A = 0;	/// Variable for organising score part 3 before transmission
uint8_t SerialTX_score_B = 0;	/// Variable for organising score part 4 before transmission
uint8_t SerialRX_control = 0;	/// Received control packets store
uint8_t SerialRX_roll = 0;		/// Received roll value store (or score part 1)
uint8_t SerialRX_pitch = 0;		/// Received pitch value store (or score part 2)
uint8_t SerialRX_score_A = 0;	/// Received score part 3 store
uint8_t SerialRX_score_B = 0;	/// Received score part 4 store
uint8_t SerialRX_end;			/// Received end packet.

// Variables for the handheld controller
uint8_t inputButtons = 0;		/// Variable of which buttons have been pressed on the controller
uint8_t inputEncoder = 0;		/// Variable of where the encoder has moved on the controller
uint8_t buttons[4];				/// Used to track history of inputButtons for debounce reasons

// Declare global variables
uint8_t randomNumber = 0;		/// Variable used to store a random number in for the handshake
uint16_t thresholdLDR;			/// Used for tracking what a "background reading" is for the LDR
uint16_t inputLDR;				/// Used for knowing the most recent scan of the LDR (only for debug)
bool mazeIsLocal = false;		/// Used to track where the maze is (TRUE for local, FALSE for remote)

// Variables for keeping track of time
unsigned long oldTime = 0;
unsigned long debugTime = 0;
unsigned long gameStart = 0;
unsigned long gameTime = 0;
unsigned long switchTime = 0;
unsigned long gameScore = 0;

// Variables for keeping track of the game
bool challenge = false;

int stateFrontend = 0;			/// Used to a tertiary state machine to track what the frontend is doing, seperate from the backend
int stateBackend = 0;			/// Used as the main state machine to track where we are in the game, what has happend and what needs to happen.
int stateMenu = 0;				/// Used to track where the user is in the menu system on the controller.
/*
STATES OF THE stateBackend STATE MACHINE
Stage = 0: Default state, no game underway
State = 1: Handshake in progress
State = 2: Setup local game
State = 3: Local Game (receive roll/pitch)
State = 4: Remote Game (transmit roll/pitch)
State = 5: reserved
Stage = 6: Game over, transmit score

STATES OF THE stateMenu STATE MACHINE
State = 00: Main Menu
State = 01:  ┣━Game Menu
State = 02:  ┃  ┣━Handshaking Screen
State = 03:  ┃  ┗━Maze Location Screen
State = 04:  ┃     ┣━Game Underway (Local)
State = 05:  ┃     ┗━Game Underway (Remote)
State = 06:  ┣━Settings Menu
State = 07:  ┃  ┣━Sensitivity Setting
State = 08:  ┃  ┗━Invert Axis Setting
State = 09:  ┗━Debug Menu
State = 10:     ┣━Display Stats
State = 11:     ┣━Test LDR/Ball sensor
State = 12:     ┗━Test IMU/Servos
State = 20: Game Menu (Waiting for game to start)
State = 21:  ┣━Remote Turn
State = 22:  ┗━Local Turn
State = 23:     ┣━Game Mode
State = 24:	    ┣━Challenge Mode
State = 25:     ┗━Game Over
*/

void setup() {
	// Initialize the lcd and turn on the backlight
	lcd.init();
	lcd.backlight();

	// Setup the two servos
	servoPitch.attach(SERVO_PITCH);
	servoRoll.attach(SERVO_ROLL);
	servo_move(0, 0);

	// Open Serial Communication
	Serial.begin(115200);				/// For Debugging from a computer (Serial 0)
	Serial1.begin(57600,SERIAL_8E1);	/// For communication with the other machine (Serial 1)
	// Communication with other is at BAUD 57600, 8-bit data, 1 start bit, 1 stop bit, one parity bit.

	// Print basic debug information
	Serial.println("Program Started");
	Serial.println("==================================================");
	Serial.println("Maze controller for SER201: Embedded System Design");
	Serial.println("Written by Alexander Gregory. (CC-BY) 2018");
	Serial.println("==================================================");

	Serial1.println("Program Started");
	Serial1.println("==================================================");
	Serial1.println("Maze controller for SER201: Embedded System Design");
	Serial1.println("Written by Alexander Gregory. (CC-BY) 2018");
	Serial1.println("==================================================");

	Serial.println("IMU device found and working with sensor ID " + String(SensorID));
	Serial.println("Using multipliers for roll/pitch: " + String(SCALE_ROLL) + "/" + String(SCALE_PITCH));
	Serial.println("Roll limits:  " + String(limitLowerRoll) + " - " + String(limitUpperRoll));
	Serial.println("Pitch limits: " + String(limitLowerPitch) + " - " + String(limitUpperPitch));
	Serial.println();
	display(100);

	// Initialize the IMU and begin receiving data
	Wire.begin();
	imu.setWire(&Wire);
	imu.beginAccel();
	SensorID = imu.readId();

	// Set one of the analogue pins to be used for random number generation
	randomSeed(analogRead(15));	/// Keep this pin unconnected/floating
	random_generate();			/// Generate our random number

	Serial.println("Generated a random number: " + String(randomNumber));

	// Turn the laser on, ready to set the threshold for the ball sensor
	pinMode(LDR, INPUT_PULLUP);
	pinMode(LASER, OUTPUT);
	digitalWrite(LASER, HIGH);

	// Give the user time to read the message on the screen
	delay(1000);

	// Get a reading for background intensity of the LDR, to set the threshold, then turn the laser off
	ldr_init();
	digitalWrite(LASER, LOW);
}

void loop() {
	// Get latest information
	poll_imu();
	poll_buttons();

	// Do all the nitty-gritty stuff
	backend();

	// Interact with the user
	frontend();

	// Rinse and repeat
}

// FUNCTION:	frontend();
// INPUTS:		nill
// OUTPUTS:		nill
// RETURNS:		nill
// PURPOSE:		Manage the front-end/user-facing stuff (Inputs, Displays, Buzzer, etc.)
void frontend() {
	// Print debug information at regular intervals (but not always)
	if ((millis() - debugTime) > DEBUG_INTERVAL) {
		debugTime = millis();
		debug();
	}

	// Load and execute the user-facing menu screens on the LCD display
	menu();
}

// FUNCTION:	backend();
// INPUTS:		nill
// OUTPUTS:		nill
// RETURNS:		nill
// PURPOSE:		Manages the backend stuff (Serial comms, IMU input, moving the maze, etc.)
void backend() {
	// Get the most recent packet information from the peer machine.
	while (serial_RX() == 1) {
		// Keep looping through this until we get a packet that is not corrupted
	}
	switch (stateBackend) {
	case 0:
		// BACKEND STATE MACHINE - STATE 0: No game underway
		// Ensure that sent packets are "clean".
		SerialTX_roll = 0;
		SerialTX_pitch = 0;
		break;
	case 1:
		// BACKEND STATE MACHINE - STATE 1: Handshakeing
		// Perform a handshake, and do different things based on the outcome of that handshake.
		switch (handshake()) {
		case 0:
			// Successfull handshake, we will play first:
			stateBackend = 2;
			// Start the game timer
			break;
		case 1:
			// Successfull handshake, opponent will play first
			stateBackend = 4;
			break;
		case 2:
			// Failed/corrupt response from peer
			break;
		}
		break;
	case 2:
		// BACKEND STATE MACHINE - STATE 2: Setup Local Game
		challenge = true;		/// Ensure that the local controller has control when the game starts (not challengem mode)
		gameStart = millis();	/// Store the time that the game starts, so that we can continually calculate the game time
		gameScore = 0;			/// Reset the game score, so we don't get confused by old values
		gameTime = 0;			/// Keep track of what the game time is. This is calculated using gameStart above
		switchTime = millis();	/// Keep track of when we need to switch the maze between local and remote control (challenge mode)
		stateBackend = 3;		/// Permanently move to the next state, as this state only needs to execute once (Will cause problems otherwise!)
	case 3:
		// BACKEND STATE MACHINE - STATE 3: Local Game

		// Calculate the score
		gameTime = millis() - gameStart;
		gameScore = calculate_score(gameTime);

		// Check if the game is over
		if (check_endgame(true)) {
			// The game is over. Calculate how long the game ran for
			gameTime = millis() - gameStart;
		} else {
			// Only run if the game is not finished

			// Check if we need to switch normal/challenge mode
			if (millis() > switchTime) {
				// We need to switch
				challenge = !challenge;						/// Switch from normal to challenge or vice versa
				if (challenge) {
					switchTime = millis() + TIME_CHALLENGE;	/// Set a new timer threshold for when to switch back
				} else {
					switchTime = millis() + TIME_NORMAL;	/// Set a new timer threshold for when to switch back
				}
			}

			if (challenge) {
				// We are in challenge mode
				stateMenu = 24;		/// Cause frontend & LCD display to update
				stateFrontend = 24;
			} else {
				// We are in normal mode
				stateMenu = 23;		/// Cause frontend & LCD display to update
				stateFrontend = 24;
			}

			// Move the servos into position if the maze is on this machine
			if (challenge) {
				// The remote machine has controll, so read from serialRX
				servo_activate(true);
			} else {
				// The local machine has control, so read from IMU
				servo_activate(false);
			}
		}
		break;
	case 4:
		// BACKEND STATE MACHINE - STATE 4: Remote Game
		// All we need to do is continually broadcast our current IMU positions, so keep them updated. This is handled by the functions
		//   that are executed before and after this state machine, so all we need to do is check for if this state is over.
		stateMenu = 21;
		check_endgame(false);
	case 6:
		// BACKEND STATE MACHINE - STATE 6: Game over
		// But the score is transmitted elsewhere, so we don't need to do much here...

		// Set the servos back to home position
		servo_move(0, 0);

		// Make sure that the laser is turned off
		digitalWrite(LASER, LOW);
		break;
	default:
		Serial.println("CRITICAL ERROR: Backend State Machine was given unknown state, " + String(stateBackend));
		break;
	}

	// Continually broadcast the game control packets (But only broadcast roll/pitch if game is running).
	switch (stateBackend) {
	case 0:
		break;
	case 1:
	case 2:
	case 3:
	case 4:
		serial_TX(false);
	case 5:
	case 6:
		serial_TX(true);
	}
}

// FUNCTION:	servo_activate();
// INPUTS:		remote
// OUTPUTS:		servo_roll, servo_pitch -> servo_move();
// RETURNS:		nill
// PURPOSE:		If passed "true" will take input from Serial RX, parse data and pass onto servo_move. If given "false" will read IMU and pass onto servo_move.
void servo_activate(bool input) {
	int8_t roll;
	int8_t pitch;
	if (input) {
		// Parse input from Serial RX
		if (SerialRX_roll > 63) {
			// The roll value is positive
			roll = SerialRX_roll - 63;
		}
		else {
			// The roll value is negative
			roll = SerialRX_roll * -1;
		}
		if (SerialRX_pitch > 63) {
			// The pitch value is positive
			pitch = SerialRX_pitch - 63;
		}
		else {
			// The pitch value is negative
			pitch = SerialRX_pitch * -1;
		}
	}
	else {
		// Convert input into format for passing to servo function
		roll = accelY * -63;
		pitch = accelX * -63;
	}
	// Pass values to servo function
	servo_move(roll, pitch);
}

// FUNCTIONN:	servo_move();
// INPUTS:		(int)roll, (int)pitch
// OUTPUTS:		nill
// RETURNS:		nill
// PURPOSE:		Takes pitch and roll, performs validity check, and converts to full range of servo movement before moving servos to that position.
void servo_move(int8_t rollInput, int8_t pitchInput) {
	// Expecting input to be a value from -63 to +63. This will need to be converted to an angle between 0 - 180 degrees, and then into the
	//   actual range permitted by the servos that won't damage the system.

	float pitch, roll;
	uint8_t pitchOutput, rollOutput;

	// Convert into range of servos
	roll = (rollInput * SCALE_ROLL) + 90;
	pitch = (pitchInput * SCALE_PITCH) + 90;

	if (roll > limitUpperRoll) {
		roll = limitUpperRoll;
	}
	else if (roll < limitLowerRoll) {
		roll = limitLowerRoll;
	}
	if (pitch > limitUpperPitch) {
		pitch = limitUpperPitch;
	}
	else if (pitch < limitLowerPitch) {
		pitch = limitLowerPitch;
	}

	// Convert floats to ints
	rollOutput = roll;
	pitchOutput = pitch;

	// Write the values to the servos
	servoPitch.write(pitchOutput);
	servoRoll.write(rollOutput);
}

// FUNCTION:	random_generate();
// INPUTS:		nill
// OUTPUTS:		randomNumber
// RETURNS:		nill
// PURPOSE:		Generate a random number and write it into the global variable "randomNumber". Number is used for serial comms.
void random_generate() {
	while (randomNumber == 0) {
		// We will generate a random number, disregarding the 3 bits that we don't need (in the control frame), and then
		//   evaluate the result to see if it is 0 (done in the "while" part). If it is zero, the process repeats until
		//   we get a non-zero random number.
		randomNumber = (0x1F & random(0, 255));
	}
}

// FUNCTION:	calculate_score();
// INPUTS:		(int)game_time, (int)takeover_count
// OUTPUTS:		nill
// RETURNS:		score
// PURPOSE:		Given game time and the number of takeovers, will calculate and return the score
unsigned long calculate_score(long gametime) {
	unsigned long score = gametime * pow(1.1,(round(gametime / TIME_CHALLENGE)));
	return(score);
}

// FUNCTION:	serial_TX_imu();
// INPUTS:		accelX, accelY
// OUTPUTS:		SerialTX_pitch, SerialTX_roll
// RETURNS:		nill
// PURPOSE:		Get the most recent IMU values, format them correctly and load them into the SerialTX values ready to be transmitted.
void serial_TX_imu() {
	// Load the IMU value into the TX value to be transmitted, and adjust the control bits in it to ensure it is valid.
	if (accelX < 0) {
		// The pitch/roll value is returned as a value from -1.00 to +1.00. This needs to be converted to the full range (-63 - +63)
		SerialTX_pitch = (accelX * -63);		/// Load the IMU value into the variable, after first converting it
		SerialTX_pitch &= 0x3F;					/// Ensure that the first two bits are 0 (control & sign)
	}
	else {
		SerialTX_pitch = (accelX / 0.015873);	/// Load the IMU value into the variable, after first converting it
		SerialTX_pitch |= 0x40;					/// Because it is positive, set the sign bit to true
		SerialTX_pitch &= 0x7F;					/// And ensure that the first bit is left zero
	}
	// And do the same for the roll axis as well...
	if (accelY < 0) {
		SerialTX_roll = (accelY * -63);
		SerialTX_roll &= 0x3F;
	}
	else {
		SerialTX_roll = (accelY / 0.015873);
		SerialTX_roll |= 0x40;
		SerialTX_roll &= 0x7F;
	}
}

// FUNCTION:	serial_TX();
// INPUTS:		serialTX_control, serialTX_pitch, serialTX_roll, serialTX_score_A, serialTX_score_b, imu
// OUTPUTS:		serialTX_control, serialTX_pitch, serialTX_roll, serialTX_score_A, serialTX_score_b
// RETURNS:		nill
// PURPOSE:		Check and modify the serialTX packets to ensure they form a valid packet (integrity check) before transmitting to the
//                other machine. If passed "true" will transmit the score instead of the IMU values (6-byte frame instead of 4).
void serial_TX(bool score) {
	if (score) {
		// Send the score

		// Ensure that the random number is written to the packet
		if ((SerialTX_control & 0b00011111) == 0) {
			SerialTX_control |= (randomNumber & 0x1F);
		}

		// Collate IMU frames & send the IMU values
		serial_TX_imu();
	}
	else {
		// Clear any current values
		SerialTX_control &= 0xE0;
		SerialTX_roll = 0;
		SerialTX_pitch = 0;
		SerialTX_score_A = 0;
		SerialTX_score_B = 0;

		// Write the score into the new values
		SerialTX_control |= (gameScore >> 27);
		SerialTX_roll |= (gameScore >> 20);
		SerialTX_pitch |= (gameScore >> 13);
		SerialTX_score_A |= (gameScore >> 6);
		SerialTX_score_B |= (gameScore);
	}

	// Ensure the packet is valid
	SerialTX_control |= 0x80;
	SerialTX_roll &= 0x7F;
	SerialTX_pitch &= 0x7F;
	SerialTX_score_A &= 0x7F;
	SerialTX_score_B &= 0x7F;

	// Send the validated packet to the remote machine
	Serial1.write(SerialTX_control);
	Serial1.write(SerialTX_roll);
	Serial1.write(SerialTX_pitch);

	// If we are in the endgame state, broadcast the extra score frames.
	if (stateBackend == 6) {
		Serial1.write(SerialTX_score_A);
		Serial1.write(SerialTX_score_B);
	}
	Serial1.write(END_FRAME);
}

// FUNCTION:	handshake();
// INPUTS:		nill
// OUTPUTS:		nill
// RETURNS:		outcome (0 == handhake complete, local first, 2 == handshake complete, remote first, 2 == failed handshake)
// PURPOSE:		Handshake with another machine over serial to get ready to start playing a game
uint8_t handshake() {
	bool local = false;
	bool remote = false;
	if (SerialRX_end != 128) {
		// We received a dodgy packet, so we can't finish the handshake
		return 2;
	}
	if ((SerialRX_control & 0x40) == 1) {
		// Check if the serial bit to indicate the opponents intention is set, and if so, update our local
		//   variable that is keeping track of this.
		remote = true;
	}
	if ((SerialTX_control & 0x40) == 1) {
		// We are using SerialTX rather than another source of this information so we are checking against the
		//   same information that we are sending to the opponent, so that there is no possibility of comming to
		//   a different conclusion than the other machine.
		local = true;
	}
	if (local == remote) {
		// Both machines wish to do the same thing, so we must check the handshake to see which gets priority
		uint8_t randomRemote = (SerialRX_control & 0x1F);
		if (randomNumber == randomRemote) {
			// Both random numbers are the same, do something!
			random_generate();
			return 2;
		}
		// Check to see who has the highest random number
		if (randomNumber > randomRemote) {
			// Our random number is greaeter than theirs, we shall go first
			return 0;
		}
		else {
			// Their random number is greater than ours, they shall go first
			return 1;
		}
	}
	else {
		// This will execute if there is no challenge of who wants to go first
		if ((local == true) && (remote == false)) {
			// We want to go first and our opponent does not, so we shall go first
			return 0;
		}
		else {
			// Our opponent wants to go first, and we do not, so we will let them go first
			return 1;
		}
	}
}

// FUNCTION:	serial_RX();
// INPUTS:		backendState;
// OUTPUTS:		SerialRX_control, SerialRX_roll, SerialRX_pitch, SerialRX_score_A, SerialRX_score_B, SerialRX_end
// RETURNS:		Success (0 == successful recieve, 1 == corrupt receive, 2 == no receive)
// PURPOSE:		Listen for a message/packet/frame from the peer and write it intoi the SerialRX variables
uint8_t serial_RX() {
	uint8_t position = 0;
	// Keep alive while serial is available
	while (Serial1.available()) {
		// Read a packet
		uint8_t packet = Serial1.read();
		// If we expect this to be the first packet, then start should be true so check if it is a valid first packet
		if (position == 0) {
			// Check if the first bit is a 1 and the remaining bits are not zero
			if (packet > 128) {
				// This is a valid start packet, so save it as such, and then continue reading the rest of the packet
				SerialRX_control = packet;
				position = 1;
			}
			else {
				// Not a valid start packet, so discard it and wait for a new one.
			}
		}
		else {
			// We do not expect this to be the first packet, read from 2 onwards:
			// Check if this is the end packet:
			if (packet == 128) {
				// This is the end packet. Save as such and move on:
				SerialRX_end = packet;
			}
			else {
				// This is not the end packet
				// Write it into the packet determined by "position".
				switch (position) {
				case 1:
					SerialRX_roll = packet;
					break;
				case 2:
					SerialRX_pitch = packet;
					break;
				case 3:
					SerialRX_score_A = packet;
					break;
				case 4:
					SerialRX_score_B = packet;
					break;
				}
				position++;
			}
		}
	}
	return 0;
}

// FUNCTION:	poll_imu();
// INPUTS:		nill
// OUTPUTS:		accelX, accelY
// RETURNS:		nill
// PURPOSE:		Handshake with another machine over serial to get ready to start playing a game
void poll_imu() {
	// Update the current IMU values
	imu.accelUpdate();
	// Get the current IMU values and store them into the gyro variables. Sensitivty control is also handled here.
	accelY = imu.accelY() * sensitivity;		/// For ROLL. Read values from the IMU and store into global IMU variable
	accelX = imu.accelX() * sensitivity * -1;	/// For PITCH. Same as above but invert the direction because the IMU is mounted upside down
	// Convert the raw float values to strings for printing to the debug log
	dtostrf(accelX, 5, 2, stringAccelX);
	dtostrf(accelY, 5, 2, stringAccelY);
}

// FUNCTION:	poll_buttons();
// INPUTS:		I2C-Arduino
// OUTPUTS:		inputButtons, inputEncoder
// RETURNS:		nill
// PURPOSE:		Connects to handhold controller and gets state of buttons
void poll_buttons() {
	// Clear the old values
	inputButtons = 0;
	// Update positions in the history
	buttons[3] = buttons[2];
	buttons[2] = buttons[1];
	buttons[1] = buttons[0];

	// Connect to remote controller over I2C
	Wire.requestFrom(I2C_SLAVE, 2);		/// Connect to the slave
	bool flag = true;					/// Keep track of which value we are receiving
	while (Wire.available()) {			/// Check if we have got a response from the slave
		if (flag) {						/// If this is the first time we have got data...
			buttons[0] = Wire.read();	/// ...store the first byte in the first variable
			buttons[0] ^= 255;				/// Invert input so a press is HIGH, and normal state is LOW
			flag = false;				/// And remember to save to the second variable next time
		}
		else {
			inputEncoder += Wire.read();	/// Save the second variable as the encoder input
		}
	}
	uint8_t i;
	for(i=0;i<8;i++){
		int a = 0;
		a += ((buttons[0] >> i) & 1);
  		a += ((buttons[1] >> i) & 1);
		a += ((buttons[2] >> i) & 1);
		a += ((buttons[3] >> i) & 1);
		if(a >= 3){
      		inputButtons |= 1 << i;
		}
	}
}

// FUNCTION:	push_buttons
// INPUTS:		button mask
// OUTPUTS:		I2C-Arduino
// RETURNS:		nill
// PURPOSE:		Used to set the LEDs built into the buttons on the remote on or off using a mask
void push_buttons(uint8_t mask) {
	// BUTTON MASK LEGEND:
	// 01: Right Button
	// 02: Centre Button
	// 04: Left Button
	// 08: Red Button
	// 16: Green Button
	Wire.beginTransmission(I2C_SLAVE);
	Wire.write(mask);
	Wire.endTransmission();
}

// FUNCTION:	check_endgame();
// INPUTS:		isLocal
// OUTPUTS:		serialTX_control, stateBackend
// RETURNS:		gameOver
// PURPOSE:		Check if a game is over. If passed true will check by testing the LDR, otherwise will check RX flag of remote game. If a game is
//                over it will determine what flags need to be set where, and change them, then return true. Otherwise it will return false.
bool check_endgame(bool isLocal) {
	if (isLocal) {
		if (check_ldr()) {
			// Local game is over, set our bit to 1
			SerialTX_control |= 0x20;
			// Is the game over or does the remote game need to run?
			if ((SerialRX_control << 5) & 1) {
				// The remote game is finished
				stateBackend = 6;
			}
			else {
				// The remote game is yet to run
				stateBackend = 4;
			}
			return true;
		}
		else {
			// Local game is still running
			return false;
		}
	}
	else {
		// We are checking the remote game
		if ((SerialRX_control & 0x20) == 32) {
			// The remote game has finished. Do we need to run a local game?
			if ((SerialTX_control & 0x20) == 32) {
				// No, our local game is finished. Proceed to end-game.
				stateBackend = 6;
			}
			else {
				// Yes, our local game now needs to run:
				stateBackend = 2;
			}
			return true;
		}
		else {
			// The remote game is still running, do nothing
			return false;
		}
	}
}

// FUNCTION:	check_ldr();
// INPUTS:		nill
// OUTPUTS:		nill
// RETURNS:		endgame
// PURPOSE:		Checks if the marble has fallen through the maze (end of game state), by polling the LDR. Will return true if ball is detected
bool check_ldr() {
	bool result;
	digitalWrite(LASER, HIGH);
	inputLDR = analogRead(LDR);
	if ((inputLDR > thresholdLDR) || (inputButtons == 32)) {
		result = true;
	} else {
		result = false;
	}
	return(result);
}

// FUNCTION:	ldr_init
// INPUTS:		nill
// OUTPUTS:		thresholdLDR
// RETUNRS:		nill
// PURPOSE:		Takes a background reading from the LDR so that we know what a high and low state is.
void ldr_init() {
	digitalWrite(LASER, HIGH);
	delay(10);
	thresholdLDR = (analogRead(LDR) + 10);
	digitalWrite(LASER, LOW);
}

// FUNCTION:	debug()
// INPUTS:		TX packets, RX packets, inputButtons, inputEncoder
// OUTPUTS:		serial
// RETURNS:		nill
// PURPOSE:		Transmit various pieces of useful information to the monitoring serial console.
void debug() {
	Serial.println("#####################");
	Serial.println("# DEBUG INFORMATION #");
	Serial.println("#####################");
	Serial.println("Sys Time:  " + String(millis()));
	Serial.println("TX Packet: " + String(SerialTX_control) + "." + String(SerialTX_roll) + "." + String(SerialTX_pitch) + "." + String(END_FRAME));
	Serial.println("RX Packet: " + String(SerialRX_control) + "." + String(SerialRX_roll) + "." + String(SerialRX_pitch) + "." + String(SerialRX_end));
	Serial.println("Inputs Btn:" + String(inputButtons) + " Encoder: " + String(inputEncoder));
	//Serial.println("IMU-var X: " + stringAccelX + " Y: " + stringAccelY);
	//Serial.print("IMU-var X: "); Serial.print(accelX, 3); Serial.print(" Y: "); Serial.println(accelY, 3);
	Serial.println("State Mch: " + String(stateFrontend) + ":" + String(stateBackend) + ":" + String(stateMenu));
	Serial.println("Random Num:" + String(randomNumber));
	Serial.println("Game Time: " + String((millis() - gameTime) / 1000) + " Switch Time: " + String((switchTime - millis()) / 1000) + " Score: " + String(gameScore));
	Serial.println("Threshold: " + String(thresholdLDR) + " Output: " + String(inputLDR));
	Serial.println("");
}

// FUNCTION:	display()
// INPUTS:		screen to display (screen), if we should continuously update
// OUTPUTS:		message on LCD screen
// PURPOSE:		Keep all the LCD printing stuff in a seperate location
void display(uint8_t screen) {
	// If the message is new, always write it to the screen
	if (screen != oldScreen) {
		// Don't get trapped in a loop!
		oldScreen = screen;

		// It's the first time displaying this screen.
		lcd.clear();	/// Clear the display
		lcd.home();		/// Return the cursor to position 0, 0.

		// Work out what we need to display, and display it.
		// (not sure why, but the order is line 1, line 3, line 2, line 4. Hence why some stuff is out of order...
		switch (screen) {
		case 0:
			// Main menu
			lcd.print("Maze Controller Menu");
			lcd.print("2) Settings         ");
			lcd.print("1) Start Game       ");
			lcd.print("3) Debug            ");
			break;
		case 1:
			// Game Starting screen
			lcd.print("Do you wish to play ");
			lcd.print("RED:   Play Second  ");
			lcd.print("  first or second?  ");
			lcd.print("GREEN: Play First   ");
			break;
		case 2:
			// Handshake screen
			lcd.print("                    ");
			lcd.print("   Remote Machine   ");
			lcd.print("  Handshaking with  ");
			lcd.print("                    ");
			break;
		case 3:
			// Location selection screen
			lcd.print(" Where is the maze? ");
			lcd.print("RED: Maze is remote ");
			lcd.print("====================");
			lcd.print("GREEN: Maze is local");
			break;
		case 6:
			// Settings Menu
			lcd.print("-= Settings  Menu =-");
			lcd.print("1)Adjust Sensitivity");
			lcd.print("                    ");
			lcd.print("2)Axis Settings     ");
			break;
		case 7:
			// Sensitivity Selection
			lcd.print("                    ");
			lcd.print("                    ");
			lcd.print("                    ");
			lcd.print("                    ");
			break;
		case 8:
			// Inverting Axis / (reserved for future use because I don't want to implement Axis Inversion)
			lcd.print("                    ");
			lcd.print("                    ");
			lcd.print("                    ");
			lcd.print("                    ");
			break;
		case 9:
			// Debug Menu
			lcd.print(" -== Debug Menu ==- ");
			lcd.print("2) Test LDR Sensor  ");
			lcd.print("1) Print Debug Info ");
			lcd.print("3) Test IMU/Servos  ");
			break;
		case 10:
			// Debug information
			lcd.print("IMU: X:      Y:     ");
			lcd.print("RX:    .            ");
			lcd.print("Btn:     Enc:       ");
			lcd.print("Packet:             ");
			break;
		case 11:
			// LDR Test
			lcd.print("   -= LDR Test =-   ");
			lcd.print("                    ");
			lcd.print("                    ");
			lcd.print("                    ");
			break;
		case 12:
			// IMU/Servo Test
			lcd.print("-= IMU/Servo Test =-");
			lcd.print("Maze should now move");
			lcd.print("to pitch position of");
			lcd.print("controller. RED=EXIT");
			break;
		case 20:
			// Game Menu (waiting for game to start)
			lcd.print("                    ");
			lcd.print("   Ready to Start   ");
			lcd.print(" Handshake Complete ");
			lcd.print("                    ");
			break;
		case 21:
			// Remote Game
			lcd.print("                    ");
			lcd.print("  on other system.  ");
			lcd.print("  Game in progress  ");
			lcd.print("                    ");
			break;
		case 22:
			// Local Game
			lcd.print("    Ready to play   ");
			lcd.print("Press GREEN to begin");
			lcd.print("   -=x=x=xx=x=x=-   ");
			lcd.print("                    ");
			break;
		case 23:
			// Playing game (normal)
			lcd.print("Game in Progress... ");
			lcd.print(" Score:             ");
			lcd.print(" Time:              ");
			lcd.print(" Takeover in:       ");
			break;
		case 24:
			// Playing game (takeover)
			lcd.print("Takeover in progress");
			lcd.print(" Time:              ");
			lcd.print(" Score:             ");
			lcd.print(" Control in:        ");
			break;
		case 25:
			// Game over
			lcd.print("                    ");
			lcd.print("        OVER        ");
			lcd.print("        GAME        ");
			lcd.print("                    ");
			break;
		case 100:
			// Loading Screen
			lcd.print("  Maze  Controller  ");
			lcd.print("for SER201: Embedded");
			lcd.print("by Alexander Gregory");
			lcd.print("System Design 2018T2");
			break;
		case 101:
			// Countdown: 1
			lcd.print("        /||         ");
			lcd.print("         ||         ");
			lcd.print("         ||         ");
			lcd.print("        _||_        ");
			break;
		case 102:
			// Countdown 2
			lcd.print("        ___         ");
			lcd.print("        / _/        ");
			lcd.print("       (__ \        ");
			lcd.print("       (____)       ");
			break;
		case 103:
			// Countdown 3
			lcd.print("        --\\        ");
			lcd.print("          \\        ");
			lcd.print("        __//        ");
			lcd.print("        __//        ");
			break;
		}
	}
}

// FUNCTIONN:	menu()
// INPUTS:		I2C-Button
// OUTPUTS:		Calls other functions
// RETURNS:		nill
// PURPOSE:		Execute the functions (case/switch) of the main menu.
void menu() {
	// First update the buttons (this will get the latest button state and write it into inputButtons, which we use multiple times in this function.
	if (inputButtons == 0) {
		if (stateMenu != stateFrontend) {
			// Next menu screen is set by stateFrontend. This will only trigger when buttons are released, so once a button is pressed it won't immediatly
			//   jump into the next menu and activate the same input on the next menu.
			stateMenu = stateFrontend;
		}
	}

	poll_buttons();
	switch (stateMenu) {
	// ####################### MAIN MENU: CASE 00: Main Menu
	case 0:
		display(0);			/// Updatae LCD Screen
		push_buttons(7);	/// Update button LEDs
		if (inputButtons == 4) {
			// User has selected "Start Game"
			stateFrontend = 1;
		}
		else if (inputButtons == 2) {
			// User has selected "options"
			stateFrontend = 6;
		}
		else if (inputButtons == 1) {
			// User has selected "Debug"
			stateFrontend = 9;
		}
		break;
	// ####################### MAIN MENU: CASE 01: Game Menu
	case 1:
		// Game Menu
		display(1);			/// Update LCD Screen
		push_buttons(24);	/// Update button LEDs
		if (inputButtons == 8) {
			// User has selected "Play Second"
			SerialTX_control &= 0xBF;
			stateMenu = 2;
			stateFrontend = 2;
			stateBackend = 1;
			delay(10);
		}
		else if (inputButtons == 16) {
			// User has selected "Play First"
			SerialTX_control |= 0x40;
			stateMenu = 2;
			stateFrontend = 2;
			stateBackend = 1;
			delay(10);
		}
		break;
	// ####################### MAIN MENU: CASE 02: Handshaking Screen
	case 2:
		display(2);			/// Update LCD Screen
		push_buttons(0);	/// Update button LEDs
		// From this point on, the menu could be forcefully navigated by the backend state machine, until the
		//   end-game state is reached. That is why there is no (button) input in this state.
		break;
	// ####################### MAIN MENU: CASE 03: Maze Location Selection
	case 3:
		display(3);			/// Update LCD Screen
		push_buttons(24);	/// Update button LEDs
		// Check buttons
		if (inputButtons == 8) {
			// User has said that maze is remote
			delay(10);
			mazeIsLocal = false;
			stateMenu = 2;
			stateBackend = 1;
			stateFrontend = 2;
		}
		else if (inputButtons == 16) {
			// User has said that the maze is local
			delay(10);
			mazeIsLocal = true;
			stateMenu = 2;
			stateBackend = 1;
			stateFrontend = 2;
		}
		break;
	// ####################### MAIN MENU: CASE 04: Game Underway (Local)
	case 4:
		display(4);			/// Update LCD Screen
		push_buttons(0);	/// Update button LEDs
		break;
	// ####################### MAIN MENU: CASE 05: Game Underway (Remote)
	case 5:
		display(5);			/// Update LCD Screen
		push_buttons(0);	/// Update button LEDs
		break;
	// ####################### MAIN MENU: CASE 06: Settings Menu
	case 6:
		display(6);			/// Update LCD Screen
		push_buttons(15);	/// Update button LEDs
		// Check for keypresses
		if (inputButtons == 4) {
			// User has selected "Sensitivity Setting"
			stateFrontend = 7;
		}
		else if (inputButtons == 2) {
			// User has selected Axis Settings
			stateFrontend = 8;
		}
		else if (inputButtons == 1) {
			// User has selected Cancel => Return to Main Menu
			stateFrontend = 0;
		}
		break;
	// ####################### MAIN MENU: CASE 07: Sensitivity Setting
	case 7:
		display(7);			/// Update LCD Screen
		push_buttons(0);	/// Update button LEDs
		if (inputButtons == 8) {
			// User has selected Cancel => Return to Main Menu
			stateFrontend = 0;
		}
		break;
	// ####################### MAIN MENU: CASE 08: Invert Axis Setting
	case 8:
		display(8);			/// Update LCD Screen
		push_buttons(0);	/// Update button LEDs
		if (inputButtons == 8) {
			// User has selected Cancel => Return to Main Menu
			stateFrontend = 0;
		}
		break;
	// ####################### MAIN MENU: CASE 09: Debug Menu
	case 9:
		display(9);			/// Update LCD Screen
		push_buttons(15);	/// Update button LEDs
		//servo_move(90, 90);	/// Reset servos to home position (if coming out of servo test)
		// Check for keypresses
		if (inputButtons == 4) {
			// User has selected "View Stats"
			stateFrontend = 10;
		}
		else if (inputButtons == 2) {
			// User has selected "Test LDR"
			stateFrontend = 11;
		}
		else if (inputButtons == 1) {
			// User has selected "Test IMU/Servo"
			stateFrontend = 12;
		}
		else if (inputButtons == 8) {
			// User has opted to return to the main menu
			stateFrontend = 0;
		}
		break;
	// ####################### MAIN MENU: CASE 10: Display Stats
	case 10:
		// Render Display
		display(10);
		push_buttons(8);

		// Update the stats on the display
		// We use if statements to make the formatting neater when negative signs are displayed
		if (accelX < 0) {
			lcd.setCursor(6, 0);
		} else {
			lcd.setCursor(7, 0);
		}
		lcd.print(String(accelX));
		if (accelY < 0) {
			lcd.setCursor(14, 0);
		} else {
			lcd.setCursor(15, 0);
		}
		lcd.print(String(accelY));

		// Print inputs from I2C Arduino
		lcd.setCursor(5, 1);
		lcd.print(String(inputButtons) + " ");
		lcd.setCursor(14, 1);
		lcd.print(String(inputEncoder));
		lcd.print("  ");

		// Print received packets
		lcd.setCursor(4, 2);
		lcd.print(String(SerialRX_control) + "." + String(SerialRX_roll) + "." + String(SerialRX_pitch) + "." + String(SerialRX_end));

		lcd.setCursor(9, 3);
		lcd.print(String(millis()));

		// Return to main menu if user presses any key
		if (inputButtons == 8) {
			// The user has pressed any key, return to main menu
			stateFrontend = 9;
		}
		break;
	// ####################### MAIN MENU: CASE 11: Test LDR / Ball Sensor
	case 11:
		display(11);		/// Update LCD Screen
		push_buttons(8);	/// Update button LEDs
		// Check for button presses
		if (inputButtons == 8) {
			stateFrontend = 9;
		}
		break;
	// ####################### MAIN MENU: CASE 12: Test IMU / Servos
	case 12:
		display(12);			/// Update LCD Screen
		push_buttons(8);		/// Update button LEDs
		servo_activate(false);	/// Moves servos to IMU position
		// Check for button presses
		if (inputButtons == 8) {
			stateFrontend = 9;
		}
		break;
	// ####################### MAIN MENU: CASE 20: Game Menu (Waiting for game to start)
	case 20:
		display(20);
		push_buttons(0);
		break;
	// ####################### MAIN MENU: CASE 21: Remote Turn (Waiting for remote game to finish)
	case 21:
		display(21);
		push_buttons(0);
		break;
	// ####################### MAIN MENU: CASE 22: Local Turn (Waiting for game to begin)
	case 22:
		display(22);
		push_buttons(0);
		break;
	// ####################### MAIN MENU: CASE 23: Local Game Playing (normal)
	case 23:
		display(23);
		push_buttons(0);
		// Keep the timers updated
		lcd.setCursor(8, 1);
		lcd.print(String(gameTime / 1000));
		lcd.setCursor(8, 2);
		lcd.print(String(gameScore));
		lcd.setCursor(14, 3);
		lcd.print(String(((switchTime - millis()) / 1000) + 1) + " ");
		break;
	// ####################### MAIN MENU: CASE 24: Local Game Playing (challenge)
	case 24:
		display(24);
		push_buttons(0);
		// Keep the timers updated
		lcd.setCursor(8, 2);
		lcd.print(String(gameTime / 1000));
		lcd.setCursor(8, 1);
		lcd.print(String(gameScore));
		lcd.setCursor(14, 3);
		lcd.print(String(((switchTime - millis()) / 1000) + 1) + " ");
		break;
	// ####################### MAIN MENU: CASE 25: Game Over
	case 25:
		display(25);
		push_buttons(0);
		break;
	}
}
