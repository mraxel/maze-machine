/*
 Name:				Maze Controller.ino
 Created:			2018-08-28 09:24:46
 Author:			Alexander Gregory
 Student ID:		217270644
 Unit Code:			SER201
 Unit Name:			Embedded System Design
 Unit Chair:		Ben Champion
 Comments:			Vars start with a lowercase letter. Functions start with an UPPERCASE letter.
*/

/*
 * This is the program for the Arduino Micro in the maze solving task
 *   solution by Alexander Gregory for SER201: Embedded System Design.
 *
 * It collects information from the user inputs (buttons and rotary
 *   encoder) and stores this information, and will return it to the
 *   master (Arduino Mega 2560 R3) when requested over an I2C bus.
 */

#define I2C_ADDRESS 9

#define BUTTON_YES A0
#define BUTTON_NO A1
#define BUTTON_LEFT A2
#define BUTTON_CENTER A3
#define BUTTON_RIGHT A4
#define BUTTON_ENCODE A5

#define ENCODER_A 7
#define ENCODER_B 8

#define LED_YES 13
#define LED_NO 12
#define LED_LEFT 11
#define LED_CENTER 10
#define LED_RIGHT 9

#include <Wire.h>

// Get global variables
volatile int8_t encode = 0;		/// This is used for tracking the encoder
uint8_t button;					/// This is used for tracking the buttons.
								///   It will be used as a collection of 8 boolean types.
uint8_t oldEncoder;				/// This is used to track when the encoder changes

unsigned long oldTime;
volatile unsigned long debounce;

void setup() {
	// Setup all the pins
	pinMode(BUTTON_YES, INPUT_PULLUP);
	pinMode(BUTTON_NO, INPUT_PULLUP);
	pinMode(BUTTON_ENCODE, INPUT_PULLUP);
	pinMode(BUTTON_LEFT, INPUT_PULLUP);
	pinMode(BUTTON_CENTER, INPUT_PULLUP);
	pinMode(BUTTON_RIGHT, INPUT_PULLUP);
	pinMode(ENCODER_A, INPUT);       /// The encoder input will have a hardware
	pinMode(ENCODER_B, INPUT);       ///   debounce and thus won't need pullup.
	pinMode(LED_YES, OUTPUT);
	pinMode(LED_NO, OUTPUT);
	pinMode(LED_LEFT, OUTPUT);
	pinMode(LED_CENTER, OUTPUT);
	pinMode(LED_RIGHT, OUTPUT);

	// Setup the I2C connection to master
	Wire.begin(I2C_ADDRESS);
	Wire.onRequest(requestEvent);         /// Functions to trigger when different
	Wire.onReceive(receiveEvent);         ///   I2C messages are received

	// Print debugging info through the serial monitor
	Serial.begin(115200);

	// Setup the encoder interrupts
	attachInterrupt(digitalPinToInterrupt(ENCODER_A), encoder, RISING);
	//attachInterrupt(ENCODER_B, EncodeB, CHANGE);
}

void loop() {
	// The main loop will check for any changes to the buttons since the last information send.
	if (digitalRead(BUTTON_YES)) {
		//Set the DigitalRead button high
		button |= 0x01;
	}
	if (digitalRead(BUTTON_NO)) {
		button |= 0x02;
	}
	if (digitalRead(BUTTON_LEFT)) {
		button |= 0x04;
	}
	if (digitalRead(BUTTON_CENTER)) {
		button |= 0x08;
	}
	if (digitalRead(BUTTON_RIGHT)) {
		button |= 0x10;
	}
	if (digitalRead(BUTTON_ENCODE)) {
		button |= 0x20;
	}
	button |= 0xC0;
	if ((millis() - oldTime) > 250) {
		oldTime = millis();
		Serial.print("B: ");
		Serial.print(button, BIN);
		Serial.print(" E: ");
		Serial.println(encode);
	}
}

void requestEvent() {
	// When asked, tell the master which buttons have been pressed
	Wire.write(button);
	Wire.write(encode);
	// Then reset the buttons for next time.
	button = 0;
	encode = 0;
	// We're done here...
}

void receiveEvent(int dataLength) {
	// This will take the information from the bus and use it to set the LEDs.
	if (dataLength == 1) {
		// All is good. Treat each bit as a boolean and use it to set LEDs on/off.
		char input = Wire.read();
		if (input & 0x01) {
			digitalWrite(LED_YES, HIGH);
		}
		else {
			digitalWrite(LED_YES, LOW);
		}
		if (input & 0x02) {
			digitalWrite(LED_NO, HIGH);
		}
		else {
			digitalWrite(LED_NO, LOW);
		}
		if (input & 0x04) {
			digitalWrite(LED_LEFT, HIGH);
		}
		else {
			digitalWrite(LED_LEFT, LOW);
		}
		if (input & 0x08) {
			digitalWrite(LED_CENTER, HIGH);
		}
		else {
			digitalWrite(LED_CENTER, LOW);
		}
		if (input & 0x10) {
			digitalWrite(LED_RIGHT, HIGH);
		}
		else {
			digitalWrite(LED_RIGHT, LOW);
		}
	}
	else {
		// Error, unknown data.
	}
}

void encoder() {
	if (digitalRead(ENCODER_B) == HIGH) {
		// A is HIGH && B is HIGH
		encode--;
	}
	else {
		// A is HIGH && B is LOW
		encode++;
	}
}